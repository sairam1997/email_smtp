import smtplib
from socket import gaierror
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from bs4 import BeautifulSoup
from flask import request, jsonify
from flask import Flask

app = Flask(__name__)


port = 2525   
smtp_server = "smtp.mailtrap.io"

login = "3bd8d0960d103c" #  login Mailtrap
password = "c55ed0f8622935" #  password Mailtrap


sender = "from@example.com"
receiver = "peralisairam1997@gmail.com"

##part1 = MIMEText(text, "plain")
#part2 = MIMEText(html, "html")
##message.attach(part1)
#message.attach(part2)




@app.route('/sendreq',methods=["POST"])
def send_req():
    data=request.get_json()
    if data["operation"]=="vkyc_reminder":
        html_vkyc_reminder="""\
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="opacity: 1;" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Video-KYC reminder</title>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <style type="text/css">
		img {
            max-width: 100%;
            }
		ol{
			padding-left: 15px;
    		margin: 0;
		}
		ol li{
		 	padding: 10px 0 5px 0px;	
		}
		ul li{
			padding: 10px 0 0px 0px;
			list-style-type:disc;	
		}
		.disc{
            border-top:1px solid #DFDEE2;
            border-bottom:1px solid #DFDEE2;
            padding: 10px 0px;
            width: 100%; 
		}
        @media only screen and (max-width: 767px) {
            .min-pad {
                padding-left: 10px !important;
				padding-right: 10px !important;
            }
		}
        @media only screen and (max-width: 480px) {
            .min-pad {
                padding-left: 5px !important;
				padding-right: 5px !important;
				font-size:12px;
            }
        }
    </style>
</head>
<body style="font-family: 'Arial'; font-size: 14px; margin: 0px; padding: 0px; color: #5a5a5a;">
   <p align="center" style="font-size: 12px;color: #54565b;margin: 15px 0px; "> If you are not able to view the content given below properly, please   <a href="#" target="_blank"> click here</a>
    </p>
    <table align="center" cellpadding="0" cellspacing="0" style="max-width:600px; min-width:280px; border:1px solid #DFDEE2;border-bottom:1px solid #DFDEE2; margin:auto;">
	<tbody>
		<tr>
			<td>
			<table class="min-pad" align="center" cellpadding="0" cellspacing="0" style=" padding:0px 20px 20px 20px; border-left:2px solid #0072bc; border-top:2px solid #0072bc;" width="100%">
			<tbody>
                <tr>
                	<td align="right" style="">
                        <a href="https://www.bajajfinserv.in" target="_blank"><img alt="Bajaj Finserv"  src="https://media.bajajfinserv.in/email/finance/mailer/2017/november/shl-loan-approval-text-mailer23-11-17/images/bfl-logo.jpg" /> </a>
                    </td>
                </tr>
                <tr>
                  <td align="left"  style="">
                    <p id="custName" style="margin:0px;padding:10px 0px 5px 0px;">Dear < Customer name >,</p>
                   
                    <p id="scheduleTime" style="margin:0px;padding:10px 0px 5px 0px;">You have an appointment scheduled at hh:mm  with us to complete your video KYC to process loan Application.</p>
<p style="margin:0px;padding:10px 0px 5px 0px;">Please click <a id="bitlyLink" href=\"{}\" > Bitly </a> to initiate video KYC .</p>

<p style="margin:0px;padding:20px 0px 0px 0px;">It is our constant endeavour to ensure you have our best attention at all times.</P>
<p style="margin:0px;padding:20px 0px 0px 0px;">Have a great day!</P>

                    <p style="margin:0px;padding:20px 0px 0px 0px;">Warm Regards,</p>
                    <p style="margin:0px;padding:20px 0px 5px 0px;"><strong>BAJAJ FINANCE LIMITED</strong></p>	

                   
                    <p style="margin:0px;padding:10px 0px 5px 0px;">This is a system generated email. Please do not respond to this email. It will not reach to any recepient.</p>									
                 									
                  </td>
              	</tr>
              	<tr>
              		<td style="padding:10px 0 0 0;" align="left">
                    	<table class="disc" cellspacing="0" style="cellpadding=0" width="100%">
                        	<tr>
                            	<td style="">
                                	<p style="margin:0px; padding:0px 5px 0px 5px;line-height:12px; background-color:#fff; font-size: 10px; "> Disbursal of your loan request is at the sole discretion of Bajaj Finance Ltd and is governed by the terms and conditions applicable to your loan request.This is a system generated alert. We request you not to reply to this message. This e-mail is confidential and may also be privileged. If you are not the intended recipient, please notify us immediately; you should not copy or use it for any purpose, nor disclose its contents to any other person.</p>
                            	</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
			</td>
		</tr>
	</tbody>	
	</table>
</body>
</html>
        
        """
        soup = BeautifulSoup(html_vkyc_reminder, "lxml")
        tag = soup.find(id="custName")
        tag.string.replace_with("Dear  {} ,".format(data['custName']))
        tag = soup.find(id="scheduleTime")
        tag.string.replace_with("You have an appointment scheduled at {scheduleTime}  with us to complete your video KYC to process loan Application.".format(scheduleTime=data['scheduleTime']))
        tag = soup.find(id="bitlyLink")
        tag['href'] = data['bitlyLink']
        final_str=str(soup)
        message = MIMEMultipart("alternative")
        message["Subject"] = "Additional information requirement for Bajaj  Loan Application"
        message["From"] = "vcip@bajajfinserv.in"
        message["To"] = data['receiver']
        part2 = MIMEText(final_str, "html")
        message.attach(part2)
    if data["operation"]=="vkyc":
        html_vkyc="""\
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="opacity: 1;" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Video-KYC</title>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <style type="text/css">
		img {
            max-width: 100%;
            }
		ol{
			padding-left: 15px;
    		margin: 0;
		}
		ol li{
		 	padding: 10px 0 5px 0px;	
		}
		ul li{
			padding: 10px 0 0px 0px;
			list-style-type:disc;	
		}
		.disc{
            border-top:1px solid #DFDEE2;
            border-bottom:1px solid #DFDEE2;
            padding: 10px 0px;
            width: 100%; 
		}
        @media only screen and (max-width: 767px) {
            .min-pad {
                padding-left: 10px !important;
				padding-right: 10px !important;
            }
		}
        @media only screen and (max-width: 480px) {
            .min-pad {
                padding-left: 5px !important;
				padding-right: 5px !important;
				font-size:12px;
            }
        }
    </style>
</head>
<body style="font-family: 'Arial'; font-size: 14px; margin: 0px; padding: 0px; color: #5a5a5a;">
   <p align="center" style="font-size: 12px;color: #54565b;margin: 15px 0px; "> If you are not able to view the content given below properly, please   <a href="#" target="_blank"> click here</a>
    </p>
    <table align="center" cellpadding="0" cellspacing="0" style="max-width:600px; min-width:280px; border:1px solid #DFDEE2;border-bottom:1px solid #DFDEE2; margin:auto;">
	<tbody>
		<tr>
			<td>
			<table class="min-pad" align="center" cellpadding="0" cellspacing="0" style=" padding:0px 20px 20px 20px; border-left:2px solid #0072bc; border-top:2px solid #0072bc;" width="100%">
			<tbody>
                <tr>
                	<td align="right" style="">
                        <a href="https://www.bajajfinserv.in" target="_blank"><img alt="Bajaj Finserv"  src="https://media.bajajfinserv.in/email/finance/mailer/2017/november/shl-loan-approval-text-mailer23-11-17/images/bfl-logo.jpg" /> </a>
                    </td>
                </tr>
                <tr>
                  <td align="left"  style="">
                    <p id="custName" style="margin:0px;padding:10px 0px 5px 0px;">Dear  Customer_name ,</p>
                   
                    <p style="margin:0px;padding:10px 0px 5px 0px;">You are just a few steps away from disbursal for your loan application.</p>
<p style="margin:0px;padding:10px 0px 5px 0px;">Click <a id="bitlyLink" href=\"{}\" > Bitly </a> to initiate process of video KYC  and fix appointment.</p>
                    
                    <p style="margin:0px;padding:20px 0px 0px 0px;"><b>Please Note</b>: This requires you to verify your Aadhar number first and then fix appointment.

<p style="margin:0px;padding:20px 0px 0px 0px;">It is our constant endeavour to ensure you have our best attention at all times.</P>
<p style="margin:0px;padding:20px 0px 0px 0px;">Have a great day!</P>

                    <p style="margin:0px;padding:20px 0px 0px 0px;">Warm Regards,</p>
                    <p style="margin:0px;padding:20px 0px 5px 0px;"><strong>BAJAJ FINANCE LIMITED</strong></p>	

                    <p style="margin:0px;padding:10px 0px 5px 0px;">This is a system generated email. Please do not respond to this email. It will not reach to any recepient.</p>									
                  </td>
              	</tr>
              	<tr> 
                    
              		<td style="padding:10px 0 0 0;" align="left">
                    	<table class="disc" cellspacing="0" style="cellpadding=0" width="100%">
                        	<tr>
                            	<td style="">
                                	<p style="margin:0px; padding:0px 5px 0px 5px;line-height:12px; background-color:#fff; font-size: 10px; "> Disbursal of your loan request is at the sole discretion of Bajaj Finance Ltd and is governed by the terms and conditions applicable to your loan request.This is a system generated alert. We request you not to reply to this message. This e-mail is confidential and may also be privileged. If you are not the intended recipient, please notify us immediately; you should not copy or use it for any purpose, nor disclose its contents to any other person.</p>
                            	</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
			</td>
		</tr>
	</tbody>	
	</table>
</body>
</html>
        """
        soup = BeautifulSoup(html_vkyc, "lxml")
        tag = soup.find(id="custName")
        tag.string.replace_with("Dear  {} ,".format(data['custName']))
        tag = soup.find(id="bitlyLink")
        tag['href'] = data['bitlyLink']
        final_str=str(soup)
        message = MIMEMultipart("alternative")
        message["Subject"] = "Additional information requirement for Bajaj  Loan Application"
        message["From"] = "vcip@bajajfinserv.in"
        message["To"] = data['receiver']
        part2 = MIMEText(final_str, "html")
        message.attach(part2)
    if data["operation"]=="vpd":
        html_vpd="""\
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="opacity: 1;" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Video-PD</title>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <style type="text/css">
		img {
            max-width: 100%;
            }
		ol{
			padding-left: 15px;
    		margin: 0;
		}
		ol li{
		 	padding: 10px 0 5px 0px;	
		}
		ul li{
			padding: 10px 0 0px 0px;
			list-style-type:disc;	
		}
		.disc{
            border-top:1px solid #DFDEE2;
            border-bottom:1px solid #DFDEE2;
            padding: 10px 0px;
            width: 100%; 
		}
        @media only screen and (max-width: 767px) {
            .min-pad {
                padding-left: 10px !important;
				padding-right: 10px !important;
            }
		}
        @media only screen and (max-width: 480px) {
            .min-pad {
                padding-left: 5px !important;
				padding-right: 5px !important;
				font-size:12px;
            }
        }
    </style>
</head>
<body style="font-family: 'Arial'; font-size: 14px; margin: 0px; padding: 0px; color: #5a5a5a;">
   <p align="center" style="font-size: 12px;color: #54565b;margin: 15px 0px; "> If you are not able to view the content given below properly, please   <a href="#" target="_blank"> click here</a>
    </p>
    <table align="center" cellpadding="0" cellspacing="0" style="max-width:600px; min-width:280px; border:1px solid #DFDEE2;border-bottom:1px solid #DFDEE2; margin:auto;">
	<tbody>
		<tr>
			<td>
			<table class="min-pad" align="center" cellpadding="0" cellspacing="0" style=" padding:0px 20px 20px 20px; border-left:2px solid #0072bc; border-top:2px solid #0072bc;" width="100%">
			<tbody>
                <tr>
                	<td align="right" style="">
                        <a href="https://www.bajajfinserv.in" target="_blank"><img alt="Bajaj Finserv"  src="https://media.bajajfinserv.in/email/finance/mailer/2017/november/shl-loan-approval-text-mailer23-11-17/images/bfl-logo.jpg" /> </a>
                    </td>
                </tr>
                <tr>
                  <td align="left"  style="">
                    <p id="custName" style="margin:0px;padding:10px 0px 5px 0px;">Dear < Customer name >,</p>
                   
                    <p style="margin:0px;padding:10px 0px 5px 0px;">We need few additional details for processing of your loan application.</p>
<p style="margin:0px;padding:10px 0px 5px 0px;">Click <a id="bitlyLink" href="lol" > Bitly </a> to initiate video call with Bajaj executive and provide required details.</p>

<p style="margin:0px;padding:20px 0px 0px 0px;">It is our constant endeavour to ensure you have our best attention at all times.</P>
<p style="margin:0px;padding:20px 0px 0px 0px;">Have a great day!</P>

                    <p style="margin:0px;padding:20px 0px 0px 0px;">Warm Regards,</p>
                    <p style="margin:0px;padding:20px 0px 5px 0px;"><strong>BAJAJ FINANCE LIMITED</strong></p>	

                   
                    <p style="margin:0px;padding:10px 0px 5px 0px;">This is a system generated email. Please do not respond to this email. It will not reach to any recepient.</p>									
                 									
                  </td>
              	</tr>
              	<tr>
              		<td style="padding:10px 0 0 0;" align="left">
                    	<table class="disc" cellspacing="0" style="cellpadding=0" width="100%">
                        	<tr>
                            	<td style="">
                                	<p style="margin:0px; padding:0px 5px 0px 5px;line-height:12px; background-color:#fff; font-size: 10px; "> Disbursal of your loan request is at the sole discretion of Bajaj Finance Ltd and is governed by the terms and conditions applicable to your loan request.This is a system generated alert. We request you not to reply to this message. This e-mail is confidential and may also be privileged. If you are not the intended recipient, please notify us immediately; you should not copy or use it for any purpose, nor disclose its contents to any other person.</p>
                            	</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
			</td>
		</tr>
	</tbody>	
	</table>
</body>
</html>
        """
        soup = BeautifulSoup(html_vpd, "lxml")
        tag = soup.find(id="custName")
        tag.string.replace_with("Dear  {} ,".format(data['custName']))
        tag = soup.find(id="bitlyLink")
        tag['href'] = data['bitlyLink']
        final_str=str(soup)
        message = MIMEMultipart("alternative")
        message["Subject"] = "Appointment for video call for Loan application {}".format(data['applicationNumber'])
        message["From"] = "vpd@bajajfinserv.in"
        message["To"] = data['receiver']
        part2 = MIMEText(final_str, "html")
        message.attach(part2)
    try:
        with smtplib.SMTP(smtp_server, port) as server:
            server.login(login, password)
            server.sendmail(sender, receiver, message.as_string()    )
            print('Sent') 
    except (gaierror, ConnectionRefusedError):
        print('Failed to connect to the server. Bad connection settings?')
        return jsonify({'msg':'ConnectionRefusedError',"status_code": 400,"success": False}), 400
    except smtplib.SMTPServerDisconnected:
        print('Failed to connect to the server. Wrong user/password?')
        return jsonify({'msg':'SMTPServerDisconnected',"status_code": 400,"success": False}), 400
    except smtplib.SMTPException as e:
        print('SMTP error occurred: ' + str(e))
        return jsonify({'msg':'SMTPException',"status_code": 400,"success": False}), 400
    return jsonify({"status_code": 200,"success": True}), 200 #'msg':final_str
        
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
    



